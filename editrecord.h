#ifndef EDITRECORD_H
#define EDITRECORD_H

#include <QWidget>

namespace Ui {
class EditRecord;
}

class EditRecord : public QWidget
{
    Q_OBJECT

public:
    explicit EditRecord(QWidget *parent = 0);
    ~EditRecord();

private slots:
    void on_pushButton_clicked();

public slots:
    void receiveData(QStringList, int);

private:
    Ui::EditRecord *ui;
    QStringList editedStringList;
    int i;
};

#endif // EDITRECORD_H
