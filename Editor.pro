#-------------------------------------------------
#
# Project created by QtCreator 2014-05-27T23:26:26
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Editor
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    addnewrecord.cpp \
    editrecord.cpp

HEADERS  += mainwindow.h \
    addnewrecord.h \
    editrecord.h

FORMS    += mainwindow.ui \
    addnewrecord.ui \
    editrecord.ui

CONFIG += mobility
MOBILITY = 

