//Класс редактирования выбраной записи

#include "editrecord.h"
#include "ui_editrecord.h"
#include <QMessageBox>
#include <QTextStream>
#include <QIODevice>
#include <QFile>

//Конструктор
EditRecord::EditRecord(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::EditRecord)
{
    ui->setupUi(this);
}

//Деструктор
EditRecord::~EditRecord()
{
    delete ui;
}


//Слот получения данных, которые отправляются с главной формы
//Получаем лист(одномерный массив, каждый элемент которого строка), и индекс выбраного элемента
void EditRecord::receiveData(QStringList stringList, int index)
{
    //присваиваем значение индекса листа переменным класса для использования
    //в методе void EditRecord::on_pushButton_clicked()
    i = index;
    editedStringList = stringList;

    //вписываем сроку которую выбрали для редактирования в компонент на форме
    ui->lineEdit->setText(editedStringList.at(index));
    this->show();
}

//Метод выполняется при нажатии по кнопке Сохранить на форме редакирования записи
void EditRecord::on_pushButton_clicked()
{
    //Считываем веденные данные из компонента lineEdit
    QString editedText = ui->lineEdit->text();
    //Заменяем выбраную запись на ту что ввели
    editedStringList.replace(i, editedText);


    //Изменения построчно ПЕРЕЗАПИСЫВАЮТСЯ в файл
    QFile file("data.txt");
    if (file.open(QIODevice::WriteOnly | QIODevice::Text)) {
        QTextStream out(&file);
        for(int i = 0; i<editedStringList.length(); i++) {
            out << editedStringList.at(i) << "\n";
        }
        file.close();
    }
    else {
        QMessageBox::warning(this, "Ошибка", "Файл не найден");
        return;
    }

    close();
}
