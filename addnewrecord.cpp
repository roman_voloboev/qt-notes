//Класс добавления новой записи в текстовый файл data.txt

#include "mainwindow.h"
#include "addnewrecord.h"
#include "ui_addnewrecord.h"
#include <QTextStream>
#include <QFile>
#include <QMessageBox>
#include <QIODevice>

//Конструктор, создает форму
AddNewRecord::AddNewRecord(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AddNewRecord)
{
    ui->setupUi(this);
}

//Деструктор
AddNewRecord::~AddNewRecord()
{
    delete ui;
}

//Метод выполняется при нажатии по кнопке Добавить на форме добавления новой записи.
void AddNewRecord::on_pushButton_clicked()
{
    QFile file("data.txt");
    //файл открывается в режиме записи и добавления новых данных в конец файла.
    if (file.open(QIODevice::WriteOnly | QIODevice::Append)) {
        QTextStream out(&file);
        //Чтение в поток введеных данных из компонента lineEdit на форме
        out << ui->lineEdit->text() << "\n";
        file.close();
        ui->lineEdit->clear();
        //Закрытие формы
        close();
    }
    else {
        QMessageBox::warning(this, "Ошибка", "Файл не найден");
        return;
    }
}
