#ifndef ADDNEWRECORD_H
#define ADDNEWRECORD_H

#include <QWidget>

namespace Ui {
class AddNewRecord;
}

class AddNewRecord : public QWidget
{
    Q_OBJECT

public:
    explicit AddNewRecord(QWidget *parent = 0);
    ~AddNewRecord();

private slots:
    void on_pushButton_clicked();

private:
    Ui::AddNewRecord *ui;
};

#endif // ADDNEWRECORD_H
