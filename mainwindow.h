#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "addnewrecord.h"
#include "editrecord.h"
#include <QMessageBox>
#include <QFocusEvent>
#include <QStringList>
#include <QStringListModel>


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
   // bool eventFilter(QObject *object, QEvent *event);

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

private slots:
    void on_pushButton_4_clicked();

signals:
    void sendData(QStringList, int);


private:
    Ui::MainWindow *ui;
    AddNewRecord addnewrecord;
    EditRecord editrecord;
    QStringList  stringList;
};

#endif // MAINWINDOW_H
