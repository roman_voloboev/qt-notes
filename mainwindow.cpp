//Класс главной формы

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFile>
#include <QTextStream>
#include <QStandardItemModel>
#include <QMessageBox>
#include <QModelIndexList>
#include <QStringList>
#include <QStringListModel>
#include <QEvent>
#include <QFocusEvent>


//Конструктор
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    EditRecord *e = new EditRecord ;
    ui->setupUi(this);
    //Устанавливаем модель данных для листа
    ui->listView->setModel(new QStringListModel(stringList));
    //Связывание сигнала со слотом, для отправки данных на другую форму
    connect(this, SIGNAL(sendData(QStringList,int)), e, SLOT(receiveData(QStringList,int)));
}


//Деструктор
MainWindow::~MainWindow()
{
    delete ui;
}

//Кнопка добавления новой записи открывает окно для добавления записи
void MainWindow::on_pushButton_clicked()
{
    addnewrecord.show();
}


//Кнопка редактирования выбраной записи, открывает окно редактирования записи
void MainWindow::on_pushButton_2_clicked()
{
    //Получаем индекс выбраной записи
    QModelIndexList selected = ui->listView->selectionModel()->selectedIndexes();
    if (!selected.isEmpty())
    {
        //Сигнал, который отсылает слоту лист с записями и индекс
        emit sendData(stringList, selected.first().row());
    }

}

//Кнопка удаления выбраной записи
void MainWindow::on_pushButton_3_clicked()
{
    //Получаем индекс выбраной записи
    QModelIndexList selected = ui->listView->selectionModel()->selectedIndexes();
    if (!selected.isEmpty())
    {
        //Удаляем из листа и обновляем модель данных на форме(список записей)
        stringList.removeAt(selected.first().row());
        ((QStringListModel*) ui->listView->model())->setStringList(stringList);

        //Записываем построчно изменения в файл
        QFile file("data.txt");
        if (file.open(QIODevice::WriteOnly | QIODevice::Text)) {
            QTextStream out(&file);
            for (int i = 0; i < stringList.size(); ++i) {
                out << stringList.at(i) << '\n';
            }
        }
        file.close();
    }
}

//Кнопка для первоначальной загрузки и последующего обновления списка с записями
void MainWindow::on_pushButton_4_clicked()
{
    //Очищаем лист от предыдущих записей
    stringList.clear();
    QFile file("data.txt");
    if (file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        QTextStream in(&file);
        //Пока не конец файла, построчно считываем и добавляем в лист
        while(!in.atEnd()) {
            stringList += in.readLine().split("\n");
        }
        file.close();        
        //Очищаем список на форме
        ui->listView->model()->removeRows(0,  ui->listView->model()->rowCount()-1);
        //Записываем в список на форме загруженные записи из файла
        ((QStringListModel*) ui->listView->model())->setStringList(stringList);
    }
    else {
        QMessageBox::warning(this, "Ошибка", "Файл не найден");
    }
}
